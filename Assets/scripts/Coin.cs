﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [Range(1, 10)][SerializeField] int cost = 1;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == GameManager.GetInstance().hero?.gameObject)
        {
            //GameManager.GetInstance().hero.SetScore(cost);
            Destroy(gameObject);
        }
    }
}
