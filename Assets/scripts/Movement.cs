﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Char ch;
    public void Init(Char c)
    {
        ch = c;
    }
    public void Move(float h, float v)
    {
        ch.state = h == 0 ? CharState.Idle : CharState.Run;
        if (h == 0) return;

        Vector3 direction = transform.right * h;
        transform.position = Vector3.MoveTowards
            (transform.position,
            transform.position + direction,
            2 * Time.deltaTime);

        transform.localScale = new Vector3(h < 0 ? -1 : 1, 1, 1);
    }
    public void Jump()
    {

        GetComponent<Rigidbody2D>()
            .AddForce(transform.up * 5,
            ForceMode2D.Impulse);
    }
}
