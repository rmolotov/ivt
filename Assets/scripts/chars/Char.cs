﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharState
{
    Idle,
    Walk,
    Run,
    Jump,
    Attack,
    Damage,
    Death
}

public class Char : MonoBehaviour
{
    public CharState state
    {
        get { return (CharState)anim.GetInteger("state"); }
        set
        {
            anim.SetInteger("state", (int)value);
        }
    }
    public Animator anim;
    public HP hp;
    public Movement movement;

    void Awake()
    {
        anim = GetComponent<Animator>();
        hp = GetComponent<HP>();

        movement = GetComponent<Movement>();
        movement.Init(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
