﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class AttackEventArgs
{
    public string message { get; }
    public int damage { get; }

    public AttackEventArgs(string s, int d)
    {
        message = s;
        damage = d;
    }
}
public class Hero : Char
{
    public UnityAction<object, AttackEventArgs> onAttack;

    public Interactable itemToInteract;
    void Start()
    {
        GameManager.GetInstance().hero = this;
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            itemToInteract?.Use();
        }
        if (Input.GetMouseButtonDown(0))
        {
            //a?.Invoke(this, new AttackEventArgs("test", 2));
            onAttack?.Invoke(this, new AttackEventArgs("test", 2));
        }
    }
}
