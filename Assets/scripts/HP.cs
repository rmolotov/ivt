﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP : MonoBehaviour
{
    [SerializeField] int MaxHealth;
    [SerializeField] Text hpText;
    [SerializeField] Image hpBar;
    int health;

    void Start()
    {
        health = MaxHealth;
        Change(this, new AttackEventArgs("", 0)); // init
    }

    public void Change(object sender, AttackEventArgs e)
    {
        health -= e.damage;
        if (hpText) hpText.text = health.ToString();
        if (hpBar) hpBar.fillAmount = (1f*health) / MaxHealth;
        if (health <= 0)
        {
            Destroy(gameObject);
        }
        print("current hp is " + health);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == GameManager.GetInstance().hero.gameObject)
        {
            GameManager.GetInstance().hero.onAttack += this.Change;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == GameManager.GetInstance().hero.gameObject)
        {
            GameManager.GetInstance().hero.onAttack -= this.Change;
        }
    }
}
