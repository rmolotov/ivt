﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject bulletPrefab;
    // Start is called before the first frame update
    [SerializeField] float cooldown;
    float timer = 0;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timer <= 0)
        {
            timer = cooldown;

            GameObject go = GameObject.Instantiate(
                bulletPrefab,
                transform.position,
                transform.rotation
                );
        }
        timer -= Time.deltaTime;
    }
}
