﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetsManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var r = Resources.Load<Texture2D>("sprites/torch");
        Resources.UnloadAsset(r);
        Sprite s = Sprite.Create(
            r,
            new Rect(0.0f, 0.0f, r.width, r.height),
            new Vector2(0.5f, 0.5f),
            100.0f);

        GetComponent<UnityEngine.UI.Image>().sprite = s;

    }
}
