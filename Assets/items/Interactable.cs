﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    Animator anim;
    [SerializeField] string Name;
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == GameManager.GetInstance().hero.gameObject)
        {
            GameManager.GetInstance().hero.itemToInteract = this;
            GameManager.GetInstance().hint.text = "Press E to use " + Name;
            GameManager.GetInstance().hint.enabled = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (GameManager.GetInstance().hero.itemToInteract == this)
            GameManager.GetInstance().hero.itemToInteract = null;
    }
    public virtual void Use()
    {
        anim?.SetBool("use", !anim.GetBool("use"));
    }

}
